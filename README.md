# Backend

## Requirements
Node version used: 8.10

Npm version used: 3.5.2

## Database
Server requires a running MongoDB daemon on localhost. Default port 27017 is used for MongoDB connection. The backend will automatically create a database with name moodDb in case it doesn't exist and a collection named moodVotes in case it doesnt exist.

## Install node dependencies

npm is used to fetch all required dependencies:

```
npm install
```

## Start backend

The backend process is started with:
```
npm start
```

## API Documentation

The API is documented with Swagger. The documentation can be accessed on endpoint /api when the backend is up and running. 
