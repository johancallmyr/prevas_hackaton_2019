const fs = require('fs')
const moment = require('moment')
const mongoClient = require('mongodb').MongoClient
const express = require('express')
const cors = require('cors')

const dbUrl = "mongodb://localhost:27017/"
const dbName = "moodDb"
const dbCollection = "moodVotes"

function saveMoodVote(moodType, timeStamp) {
    mongoClient.connect(dbUrl, function (err, db) {
        if (err) {
            throw err
        }

        var dbo = db.db(dbName)
        var moodObj = {mood: moodType, time: timeStamp}
        dbo.collection(dbCollection).insertOne(moodObj, function (err, res) {
            if (err) {
                throw err
            }
            console.log("Mood " + moodType + " inserted at time " + timeStamp)
            db.close()
        })
    })
}

function getMoodVotes(startTime, endTime) {
    return new Promise(function (resolve, reject) {
        mongoClient.connect(dbUrl, (err, db) => {
            if (err) {
                reject(err)
            }
            var dbo = db.db(dbName)
            var filter = {"time": {$gte: startTime, $lte: endTime}}
            dbo.collection(dbCollection).find(filter, {
                projection: {
                    _id: 0,
                    mood: 1,
                    time: 1
                }
            }).toArray().then((result) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(result);
                }

                db.close()
            })
        })
    });
}

app = express()
app.use(cors())

// Setup routing of rest endpoints
app.post('/mood/happy', (req, res) => {
    saveMoodVote("happy", moment().unix())
    res.sendStatus(200)
})

app.post('/mood/neutral', (req, res) => {
    saveMoodVote("neutral", moment().unix())
    res.sendStatus(200)
})

app.post('/mood/angry', (req, res) => {
    saveMoodVote("angry", moment().unix())
    res.sendStatus(200)
})

app.get('/mood', (req, res) => {
    var startTime = 0
    var endTime = moment().unix()

    if(req.query.startTime) {
        startTime = moment(req.query.startTime).unix();
    }
    if(req.query.endTime) {
        endTime = moment(req.query.endTime).unix();
    }

    console.log("Get mood stats from " + startTime + " to " + endTime)

    var moodRequest = getMoodVotes(startTime, endTime).then((result) => {
        res.send(JSON.stringify(result))
    });
})

// Setup swagger for API documentation
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./swagger.json');
app.use('/api', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

// Start listening for rest calls
const port = 9000
app.listen(port)

console.log('Mood RESTful API server started on: ' + port)
